# Simple Sitemap manager for large sitemaps

Generates main sitemap index file, subdirectory with sitemaps, defaults 5000 max lines each.
After creating deletes old sitemap files if any and replaces them with newly generated ones.

Usage Example:

    use dubinsky\sitemap\SitemapManager;
    
    $domain = "test.com";
    
    $sitemapManager = new SitemapManager($domain, __DIR__.'/html/', 5000);
    $sitemapManager->appendLine('<url><loc>'.$domain.'/</loc></url>');
    $sitemapManager->flush();
    
in case you need custom paths for sitemap folder and index files

    $domain = "http://test_es.com";
    $sitemapManager = new SitemapManager($domain, __DIR__.'/html/', 5000);
    $sitemapManager->setSitemapIndexFilename('sitemap_es.xml');
    $sitemapManager->setSitemapFolder('sitemap/es/');//physical folder location
    $sitemapManager->setSitemapWebFolder('sitemap/');//outputs http://test_es.com/sitemap/1.xml in sitemap index
    $sitemapManager->appendLine('<url><loc>'.$domain.'/</loc></url>');
    $sitemapManager->flush();
    