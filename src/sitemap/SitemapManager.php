<?php

namespace dubinsky\sitemap;

class SitemapManager {

    private $domain;

    private $maxLines;

    private $rootPath;

    private $page = 1;

    /**
     * @var SitemapFile
     * 
     * current sitemap to write. will be created new when full.
     */
    private $sitemap;

    /**
     * @var resource sitemap index (map of sitemaps) file
     */
    private $sitemapIdxFile;

    /**
     * @var string sitemap index temporary filename
     */
    private $sitemapIdxTmpName;

    /**
     * @var string sitemap index filename
     */
    private $sitemapIdxName;

    /**
     * @var string folder where temporary sitemaps are created
     */
    private $sitemapTmpFolder;

    /**
     * @var string
     */
    private $sitemapFolder;
    
    /**
     * @var string 
     * sitemap folder will be used in sitemap index file in case folders where they are physically located are symlinks
     * by default this folder is same as $sitemapFolder.
     * 
     * domain + sitemapIdxFolder + page.xml
     */
    private $sitemapIdxWebFolder;

    /**
     * @param $domain - your website domain
     * @param $rootPath - root path for a sitemap index file
     * @param int $maxLines - max lines for each single sitemap file
     * @throws \Exception
     */
    public function __construct($domain, $rootPath,  $maxLines = 5000) {
        $this->domain = trim($domain, "/");
        $this->maxLines = $maxLines;
        $this->rootPath = rtrim($rootPath, "/");
        //prepare sitemap folder
        if (!file_exists($this->rootPath)) {
            mkdir($this->rootPath, 0755, true);
        }
        $this->setSitemapIndexFilename('sitemap.xml');
        $this->setSitemapFolder('sitemap/');
    }

    public function appendLine($line) {
        if($this->sitemap  == null) {
            $this->sitemap = $this->prepareNewXmlFile($this->page);
        }
        $this->sitemap->appendLine($line);
        if($this->sitemap->getLineCount() > $this->maxLines) {
            $this->sitemap->flush();
            $this->sitemap = null;
            $this->page++;
        }
    }

    public function flush() {
        if($this->sitemap) {
            $this->sitemap->flush();
        }

        $this->finaliseSitemapIndex();

        //delete old sitemap folder
        $this->deleteDir($this->sitemapFolder);

        //rename new files back
        if(!rename($this->sitemapTmpFolder, $this->sitemapFolder)) {
            throw new \Exception("Failed to rename sitemap folder $this->sitemapTmpFolder, $this->sitemapFolder");
        }
        if (file_exists($this->sitemapIdxName)) {
            if (!unlink($this->sitemapIdxName)) {
                throw new \Exception("failed to delete old sitemap");
            }
        }

        if (!rename($this->sitemapIdxTmpName, $this->sitemapIdxName)) {
            throw new \Exception("failed to rename sitemap");
        }
    }

    /**
     * Set custom index filename.
     * Should be called before first call of appendLine method!
     * @param $sitemapIndexFilename - filename for root sitemap file
     */
    public function setSitemapIndexFilename($sitemapIndexFilename) {
        if ($this->isRelativePath($sitemapIndexFilename)) {
            $sitemapIndexFilename = $this->rootPath.'/'.$sitemapIndexFilename;
        }
        $this->sitemapIdxName = $sitemapIndexFilename;
        $this->sitemapIdxTmpName = $this->sitemapIdxName.'.tmp';
    }

    /**
     * set custom folder, automatically updates sitemap web folder
     * @param string $sitemapFolderName
     */
    public function setSitemapFolder($sitemapFolderName) {
        $sitemapFolderName = rtrim($sitemapFolderName, "/");
        if ($this->isRelativePath($sitemapFolderName)) {
            $sitemapFolderPath = $this->rootPath.'/'.$sitemapFolderName;
        } else {
            $sitemapFolderPath = $sitemapFolderName;
        }
        $this->sitemapFolder = $sitemapFolderPath."/";
        $this->sitemapTmpFolder = $sitemapFolderPath."_tmp/";
        $this->setSitemapWebFolder($sitemapFolderName);
    }
    
    /**
     * sets sitemap web folder path (one that is written in sitemap index file) may differs in case of symlinked folders
     * @param string $sitemapFolderName
     */
    public function setSitemapWebFolder($sitemapFolderName) {
        if ($this->isRelativePath($sitemapFolderName)) {
            $this->sitemapIdxWebFolder = $this->domain."/".$sitemapFolderName;
        } else {
            $this->sitemapIdxWebFolder = $sitemapFolderName;
        }
        $this->sitemapIdxWebFolder = rtrim($this->sitemapIdxWebFolder, "/");
        $this->sitemapIdxWebFolder = $this->sitemapIdxWebFolder."/";
    }

    private function prepareSitemapIndex() {
        $this->sitemapIdxFile = fopen($this->sitemapIdxTmpName, "w");
        if (!$this->sitemapIdxFile) {
            throw new \Exception( "error opening ".$this->sitemapIdxTmpName);
        }
        $sitemap_index_header =
'<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        fwrite($this->sitemapIdxFile, $sitemap_index_header . PHP_EOL);
    }

    /**
     * Prepare new sitemap subxml
     */
    private function prepareNewXmlFile($page) {
        if (!file_exists($this->sitemapTmpFolder)) {
            if (!mkdir($this->sitemapTmpFolder,0777, true)) {
                throw new \Exception("failed to create directory sitemap_bak");
            }
        }
        if (!$this->sitemapIdxFile) {
            $this->prepareSitemapIndex();
        }
        fwrite($this->sitemapIdxFile, '<sitemap><loc>'.$this->sitemapIdxWebFolder.$page.'.xml</loc></sitemap>'.PHP_EOL);
        $sitemap = new SitemapFile($this->sitemapTmpFolder.$page.'.xml');
        return $sitemap;
    }

    private function finaliseSitemapIndex() {
        if (!$this->sitemapIdxFile) {
            $this->prepareSitemapIndex();
        }
        fwrite($this->sitemapIdxFile, '</sitemapindex>' . PHP_EOL);
        if (!fclose($this->sitemapIdxFile)) {
            throw new \Exception("failed to close sitemap index");
        }
    }

    private function deleteDir($dirPath) {
        if (! is_dir($dirPath)) {
            return;
//if dir is not empty ignore it        throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                $this->deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }
    
    private function isRelativePath($path) {
        return (substr($path, 0, 1) != "/");
    }
}