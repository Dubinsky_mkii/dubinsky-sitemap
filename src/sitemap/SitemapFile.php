<?php

namespace dubinsky\sitemap;

class SitemapFile {

    private $line = 1;

    private $sitemap;

    private $filename;

    public function __construct($filename) {
        $this->filename = $filename;
        $this->sitemap = fopen($this->filename, "w");
        if (!$this->sitemap) {
            //finaliseSitemapIndex($sitemap_index);
            throw new \Exception("error opening ".$this->filename);
        }
        $xml_header =
'<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        fwrite($this->sitemap, $xml_header . PHP_EOL);
    }

    public function appendLine($line) {
        fwrite($this->sitemap, $line.PHP_EOL);
        $this->line++;
    }

    public function getLineCount() {
        return $this->line;
    }

    public function flush() {
        $xml_footer = '</urlset>';
        fwrite($this->sitemap, $xml_footer);
        if (!fclose($this->sitemap)) {
            throw new \Exception("failed to close sitemap file ".$this->filename);
        }
    }
}