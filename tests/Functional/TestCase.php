<?php

namespace Tests\Functional;

use dubinsky\sitemap\SitemapManager;

define('TEST_PATH', __DIR__.'/../testbed/'); 

class TestCase extends \PHPUnit_Framework_TestCase
{
    
    private $baseTestPath = __DIR__.'/../testbed/';
    
    /**
     * php vendor/bin/phpunit tests/Functional/TestCase.php
     */
    public function testSimpleSitemap()
    {
        $domain = "test.com";
        $sitemapManager = new SitemapManager($domain, TEST_PATH, 50);
        $sitemapManager->appendLine('<url><loc>'.$domain.'/</loc></url>');
        
        $this->assertFileExists(TEST_PATH.'/sitemap.xml.tmp');
        
        $sitemapManager->flush();
        
        $this->assertFileExists(TEST_PATH.'/sitemap.xml');
        $this->assertFileEquals(__DIR__.'/../reference/sitemap.xml', TEST_PATH.'/sitemap.xml');
        $this->assertFileNotExists(TEST_PATH.'/sitemap.xml.tmp');
    }
    
    public function testCustomSitemap()
    {
        $domain = "test.com";
        $sitemapManager = new SitemapManager($domain, TEST_PATH, 50);
        $sitemapManager->setSitemapIndexFilename('sitemap_es.xml');
        $sitemapManager->setSitemapFolder('sitemap/es/');
        $sitemapManager->appendLine('<url><loc>'.$domain.'/</loc></url>');
        
        $this->assertFileExists(TEST_PATH.'/sitemap_es.xml.tmp');
        
        $sitemapManager->flush();
        
        $this->assertFileEquals(__DIR__.'/../reference/sitemap_es.xml', TEST_PATH.'/sitemap_es.xml');
        $this->assertFileEquals(__DIR__.'/../reference/1.xml', TEST_PATH.'/sitemap/es/1.xml');
        $this->assertFileExists(TEST_PATH.'/sitemap_es.xml');
        $this->assertFileNotExists(TEST_PATH.'/sitemap_es.xml.tmp');
    }
    
    public function testCustomWebSitemap()
    {
        $domain = "test.com";
        $sitemapManager = new SitemapManager($domain, TEST_PATH, 50);
        $sitemapManager->setSitemapIndexFilename('sitemap_linked.xml');
        $sitemapManager->setSitemapFolder('sitemap/linked/');
        $sitemapManager->setSitemapWebFolder('sitemap/notlinked/');
        $sitemapManager->appendLine('<url><loc>'.$domain.'/</loc></url>');
        
        $this->assertFileExists(TEST_PATH.'/sitemap_linked.xml.tmp');
        
        $sitemapManager->flush();
        
        $this->assertFileEquals(__DIR__.'/../reference/sitemap_linked.xml', TEST_PATH.'/sitemap_linked.xml');
        $this->assertFileExists(TEST_PATH.'/sitemap_linked.xml');
        $this->assertFileNotExists(TEST_PATH.'/sitemap_linked.xml.tmp');
    }
}
